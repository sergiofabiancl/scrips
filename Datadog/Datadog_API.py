# %%
import pandas as pd
from datadog_api_client import ApiClient, Configuration
from datadog_api_client.v1.api.monitors_api import MonitorsApi
from datadog_api_client.v1.model.monitor_options import MonitorOptions
from datadog_api_client.v1.model.monitor_thresholds import MonitorThresholds
from datadog_api_client.v1.model.monitor_update_request import MonitorUpdateRequest
from datadog_api_client.v1.api.authentication_api import AuthenticationApi
from datadog_api_client.v1.model.monitor import Monitor
from datadog_api_client.v1.model.monitor_type import MonitorType

# %%
# VARIABLES Necesarias "DD_API_KEY" y "DD_APP_KEY"
#%env DD_API_KEY=""
#%env DD_APP_KEY=""

# %%
def test_credentials():
  configuration = Configuration()
  with ApiClient(configuration) as api_client:
      api_instance = AuthenticationApi(api_client)
      response = api_instance.validate()
      
      return response

print(test_credentials())

# %%
def get_monitor_detail(monitor_id):
  configuration = Configuration()
  with ApiClient(configuration) as api_client:
      api_instance = MonitorsApi(api_client)
      response = api_instance.get_monitor(
          monitor_id=int(monitor_id),
          with_downtimes=True,
      )
      return response

get_monitor_detail(000000000)

# %%
def get_all_monitors():
  configuration = Configuration()
  with ApiClient(configuration) as api_client:
      api_instance = MonitorsApi(api_client)
      response = api_instance.list_monitors()

      return response

# %%
def modify_add_timeout(alert_id):
    body = MonitorUpdateRequest(
        options=MonitorOptions(
            timeout_h=1,
        ),
    )
    configuration = Configuration()
    with ApiClient(configuration) as api_client:
        api_instance = MonitorsApi(api_client)
        try:
            response = api_instance.update_monitor(monitor_id=int(alert_id), body=body)
            print(response)
            print("Exito")
        except:
            print("Fallo")

# %%
def modify_notify_audit(alert_id):
    body = MonitorUpdateRequest(
        options=MonitorOptions(
            notify_audit=False,
        ),
    )
    configuration = Configuration()
    with ApiClient(configuration) as api_client:
        api_instance = MonitorsApi(api_client)
        response = api_instance.update_monitor(monitor_id=int(alert_id), body=body)
        
        return(response)

modify_notify_audit(000000000)

# %%
def delete_alert(alert_id):
  configuration = Configuration()
  with ApiClient(configuration) as api_client:
      api_instance = MonitorsApi(api_client)
      response = api_instance.delete_monitor(monitor_id=int(alert_id),)

      return(response)

#delete_alert(00000000)

# %%
def run_alert_list():
    df = pd.read_csv("/.../alertas.csv", usecols=['ID'])
    for item in df.index:
        alert_id = df["ID"][item]
        print(f"Trabajando en {alert_id}...")
        try:
            #modify_alert(alert_id)
            print(delete_alert(alert_id))
            print("Success")
        except:
            print("Fail")

            
run_alert_list()


