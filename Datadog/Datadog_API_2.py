# %%
from datadog_api_client import ApiClient, Configuration
from datadog_api_client.v1.api.monitors_api import MonitorsApi
from datadog_api_client.v1.model.monitor_update_request import MonitorUpdateRequest

# %%
#%env DD_API_KEY=
#%env DD_APP_KEY=

# %%

# AQUI DEFINIR FILTRO DE ALERTAS
busqueda = "team:mkp_sre"

# AQUI INDICAR EL TAG A AGREGAR
tag = "team:drb_devops"

##
configuration = Configuration()
with ApiClient(configuration) as api_client:
    api_instance = MonitorsApi(api_client)
    response = api_instance.search_monitors(query=busqueda,per_page=400)
    print(f"Se encontraron {len(response.monitors)} alertas")
    for monitor in response.monitors:
      monitor.tags.append(tag)
      body = MonitorUpdateRequest(
        tags=monitor.tags
      )
      try:
        api_instance.update_monitor(monitor_id=monitor.id, body=body)
        print(f"Alerta {monitor.id} modificada exitosamente!")
      except:
        print(f"Alerta {monitor.id} fallo")



