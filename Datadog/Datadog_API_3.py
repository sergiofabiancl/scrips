# %%
import pandas as pd
from datadog_api_client import ApiClient, Configuration
from datadog_api_client.v1.api.monitors_api import MonitorsApi
from datadog_api_client.v1.model.monitor_update_request import MonitorUpdateRequest
from datadog_api_client.v1.api.authentication_api import AuthenticationApi


# %%
#%env DD_API_KEY=
#%env DD_APP_KEY=

# %%
mandatory_message = '\n{{#is_alert}}\n@teams-SRE-OPERATION \n@slack-Falabella-sre_india_team \n@webhook-victorops-sre-operation \n@webhook-victorops-sre-team-mkp-alarm \n{{/is_alert}} \n\n{{#is_alert_recovery}}\n@teams-SRE-OPERATION \n@slack-Falabella-sre_india_team \n@webhook-victorops-sre-operation-resolve \n@webhook-victorops-sre-team-mkp-resolve \n{{/is_alert_recovery}}'

# %%
def test_credentials():
  configuration = Configuration()
  with ApiClient(configuration) as api_client:
      api_instance = AuthenticationApi(api_client)
      response = api_instance.validate()
      
      return response

print(test_credentials())

# %%
def get_monitor_detail(monitor_id):
  configuration = Configuration()
  with ApiClient(configuration) as api_client:
      api_instance = MonitorsApi(api_client)
      response = api_instance.get_monitor(
          monitor_id=int(monitor_id)
      )
      message = response["message"]
      return message

get_monitor_detail(148900268)

# %%
def notify_team(message):
  print("Chequeando receptores...")
  contacts = [
    "teams-SRE-OPERATION", 
    "slack-Falabella-sre_india_team", 
    "webhook-victorops-sre-operation", 
    "webhook-victorops-sre-team-mkp-alarm"
    ]
  for item in contacts:
    if item not in message:
      print(f"Receptor {item} no listado en alerta")
      return False
  return True


# %%
def modify_message(monitor_id, old_message):
    body = MonitorUpdateRequest(
        message= old_message + mandatory_message
    )
    configuration = Configuration()
    with ApiClient(configuration) as api_client:
        api_instance = MonitorsApi(api_client)
        try:
            response = api_instance.update_monitor(monitor_id=int(monitor_id), body=body)
        except:
            print("Modificacion de monitor " + monitor_id + " fallo!")
            return "Fallo!"
        return "Exito!"


# %%
def process_monitor(monitor_id):
  configuration = Configuration()
  with ApiClient(configuration) as api_client:
      api_instance = MonitorsApi(api_client)
      response = api_instance.get_monitor(
          monitor_id=int(monitor_id)
      )
      old_message = response["message"]
      result = notify_team(old_message)
      if result == False:
        print("Monitor " + str(monitor_id) + " sera modificado")
        result = modify_message(monitor_id, old_message)
      else:
        print("Nada que hacer")
  return "Proceso terminado"

#print(process_monitor(148900268))

# %%
def run_alert_list():
    df = pd.read_csv("/Users/sgarcesr/Desktop/Lab/Personal/scrips/alertas.csv", usecols=['ID']) 
    for item in df.index:
        alert_id = df["ID"][item]
        print(f"Trabajando en {alert_id}...")
        try:
            process_monitor(alert_id)
            print("Success")
        except:
            print("Fail")

run_alert_list()

