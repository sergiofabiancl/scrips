# %%
from datadog_api_client import ApiClient, Configuration
from datadog_api_client.v1.api.monitors_api import MonitorsApi
from datadog_api_client.v1.model.monitor_update_request import MonitorUpdateRequest
from datadog_api_client.v1.api.authentication_api import AuthenticationApi

import csv
import json

# %%
#%env DD_API_KEY=
#%env DD_APP_KEY=

# %%
def test_credentials():
  configuration = Configuration()
  with ApiClient(configuration) as api_client:
      api_instance = AuthenticationApi(api_client)
      response = api_instance.validate()
      
      return response

print(test_credentials())

# %%
# AQUI DEFINIR FILTRO DE ALERTAS
busqueda = "team:drb_devops sodimac"

# %%
configuration = Configuration()
with ApiClient(configuration) as api_client:
    api_instance = MonitorsApi(api_client)
    response = api_instance.search_monitors(query=busqueda,per_page=400)
    print(f"Se encontraron {len(response.monitors)} monitores")
    #print(response)
    with open('/Users/sgarcesr/Desktop/Lab/Personal/scrips/monitores.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        for monitor in response.monitors:
            monitor_name = monitor.name
            monitor_id = monitor.id
            writer.writerow([monitor_id, monitor_name])
    


