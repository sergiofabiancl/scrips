# %%
import pandas as pd
from google.auth import default
from google.cloud import container_v1, bigquery
import subprocess
import sys
from kubernetes import client, config
import re
import json
from datetime import datetime

# %%
credentials, project = default()
query = "SELECT * FROM `prod-observability.cost_observability.gke_clusters`"
df_clusters = pd.read_gbq(query, project_id='prod-observability', credentials=credentials)

# %%
def define_region(zones):
    zone_list = zones.split(',')
    zone = zone_list[0]
    region = zone[:-2]
    return region

# %%
def create_bigquery_table(df, table_id):
    client = bigquery.Client()
    print(f"Creating table {table_id}")
    job_config = bigquery.LoadJobConfig(write_disposition="WRITE_TRUNCATE")
    job = client.load_table_from_dataframe(df, table_id, job_config=job_config)
    job.result()

# %%
def get_gke_credentials(cluster_name, region, project_id, max_retries=3):
    command = [
        "gcloud", "container", "clusters", "get-credentials",
        cluster_name, "--region", region, "--project", project_id
    ]
    retries = 0
    while retries < max_retries:
        try:
            subprocess.run(command, check=True, stderr=subprocess.PIPE)
            print(f"Successfully accessed cluster: {cluster_name}")
            return True
        except subprocess.CalledProcessError as e:
            error_output = e.stderr.decode("utf-8")
            if "Did you mean" in error_output:
                match = re.search(r"Did you mean \[(.*?)\] in \[(.*?)\]", error_output)
                if match:
                    suggested_region = match.group(2)
                    print(f"Error: {e}. Trying with suggested region: {suggested_region}")
                    region = suggested_region
                    retries += 1
            else:
                print(f"Error: {e}. Failed to get GKE credentials for cluster: {cluster_name}")
                return False
    print(f"Exceeded maximum retries ({max_retries}). Failed to get GKE credentials for cluster: {cluster_name}")
    return False

# %%
def parse_date(date_string): 
    return datetime.strptime(date_string, "%Y-%m-%dT%H:%M:%SZ")

# %%
columns = ['PROJECT',
           'ENV',
           'CLUSTER_NAME',
           'NAMESPACE',
           'DEPLOYMENT',
           'DEPLOYMENT_MIN_CPU',
           'DEPLOYMENT_MAX_CPU',
           'DEPLOYMENT_MIN_MEM',
           'DEPLOYMENT_MAX_MEM',
           'HPA_NAME',
           'HPA_MIN_PODS',
           'HPA_MAX_PODS',
           'REPLICAS',
           'STATUS',
           'MESSAGE',
           'REASON',
           'LAST_TRANSITION_TIME'
           ]

# %%
df = pd.DataFrame(columns=columns)
for index, row in df_clusters[['cluster_name', 'project_id', 'locations', 'env']].iterrows():
    region = define_region(row['locations'])
    cluster_ready =  get_gke_credentials(cluster_name=row['cluster_name'], region=region, project_id=row['project_id'])
    if cluster_ready == False:
        continue

    config.load_kube_config()
    v1 = client.CoreV1Api()
    autoscaling_v1 = client.AutoscalingV1Api()
    apps_v1 = client.AppsV1Api()

    try:
        namespaces = v1.list_namespace().items

        for ns in namespaces:
                deployments = apps_v1.list_namespaced_deployment(ns.metadata.name).items

                for deploy in deployments:
                    hpas = autoscaling_v1.list_namespaced_horizontal_pod_autoscaler(ns.metadata.name).items

                    for hpa in hpas:
                        if hpa.metadata.name == deploy.metadata.name:

                            deployment_min_cpu = None
                            try:
                                deployment_min_cpu = deploy.spec.template.spec.containers[0].resources.requests['cpu']
                            except:
                                pass

                            deployment_limit_cpu = None
                            try:
                                deployment_limit_cpu = deploy.spec.template.spec.containers[0].resources.limits['cpu']
                            except:
                                pass

                            deployment_min_memory = None
                            try:
                                deployment_min_memory = deploy.spec.template.spec.containers[0].resources.requests['memory']
                            except:
                                pass

                            deployment_limit_memory = None
                            try:
                                deployment_limit_memory = deploy.spec.template.spec.containers[0].resources.limits['memory']
                            except:
                                pass

                            if hpa.spec.min_replicas != hpa.spec.max_replicas:
                                if hpa.spec.max_replicas == hpa.status.current_replicas:
                                    status="MS and HPA need to be analyzed"
                                else:
                                    status="OK"
                            else:
                                status = "HPA misconfigured"
                            
                            message = None
                            reason = None
                            last_transition_time = None
                            try:
                                json_string = hpa.metadata.annotations['autoscaling.alpha.kubernetes.io/conditions']
                                data = json.loads(json_string)
                                latest_entry = max(data, key=lambda x: parse_date(x["lastTransitionTime"])) 
                                message = latest_entry["message"] 
                                reason = latest_entry["reason"]
                                last_transition_time = latest_entry["lastTransitionTime"]
                            except (KeyError, ValueError, TypeError) as e:
                                print(f"Error obteniendo conditions: {e}")
                                pass
                                
                            df.loc[df.shape[0]] = {
                                'PROJECT': row['project_id'],
                                'ENV': row['env'],
                                'CLUSTER_NAME': row['cluster_name'],
                                'NAMESPACE': ns.metadata.name,
                                'DEPLOYMENT': deploy.metadata.name,
                                'DEPLOYMENT_MIN_CPU': deployment_min_cpu,
                                'DEPLOYMENT_MAX_CPU': deployment_limit_cpu,
                                'DEPLOYMENT_MIN_MEM': deployment_min_memory,
                                'DEPLOYMENT_MAX_MEM': deployment_limit_memory,
                                'HPA_NAME': hpa.metadata.name,
                                'HPA_MIN_PODS': hpa.spec.min_replicas,
                                'HPA_MAX_PODS': hpa.spec.max_replicas,
                                'REPLICAS': hpa.status.current_replicas,
                                'STATUS': status,
                                'MESSAGE': message,
                                'REASON': reason,
                                'LAST_TRANSITION_TIME': last_transition_time
                            }
    except Exception as ex:
        print(ex)
        pass

                    
print(df)
create_bigquery_table(df=df,table_id="prod-observability.cost_observability.gke_clusters_hpa")


# %%
get_gke_credentials("sodimac-search-prod-cluster", "us-east4", "sodimac-search-prod")

config.load_kube_config()
v1 = client.CoreV1Api()
autoscaling_v1 = client.AutoscalingV1Api()
apps_v1 = client.AppsV1Api()

#namespaces = v1.list_namespace().items
#print(namespaces)

#deployments = apps_v1.list_namespaced_deployment("search").items
#print(deployments)

hpas = autoscaling_v1.list_namespaced_horizontal_pod_autoscaler("search").items
for hpa in hpas:
  message = None
  reason = None
  last_transition_time = None
  try:
      json_string = hpa.metadata.annotations['autoscaling.alpha.kubernetes.io/conditions']
      data = json.loads(json_string)
      #latest_entry = max(data, key=lambda x: parse_date(x["lastTransitionTime"]))
      scaling_limited_entry = next((item for item in data if item["type"] == "ScalingLimited"), None)
      message = scaling_limited_entry["message"] 
      reason = scaling_limited_entry["reason"]
      type_status = scaling_limited_entry["status"]
      last_transition_time = scaling_limited_entry["lastTransitionTime"]
  except (KeyError, ValueError, TypeError) as e:
      print(f"Error obteniendo conditions: {e}")
      pass
  print(f"{reason} - {type_status}")



